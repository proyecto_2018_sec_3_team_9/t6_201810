package model.data_strucutres.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.LinearProbingHash;
import model.vo.Taxi;

public class LinearProbingHashTest<Key, Value> 
{
	private LinearProbingHash<Key, Value> linearHash;
	Value taxi = (Value) new Taxi("id","company");
	Key llave = (Key) "llave";
	
	Value taxi2 = (Value) new Taxi("id2","compañia2");
	Key llave2 = (Key) "llave2";

	/**
	 * Creates a new empty LinearProbing HT. 
	 */
	@Before
	public void setupEscenario1()
	{
		linearHash = new LinearProbingHash<>();
	}

	/**
	 * Test 1: Verifies method put(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 */
	@Test
	public void testPut()
	{
		//Primer escenario con una linear probing HT
		setupEscenario1();
		
		linearHash.put(llave, taxi);
		assertEquals(1, linearHash.size());
	}

	/**
	 * Test 2: Verifies method get(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * get<br>
	 */
	@Test
	public void testGet()
	{
		//Primer escenario con una linear probing HT
		setupEscenario1();
		linearHash.put(llave, taxi);	
		assertEquals(taxi, linearHash.get(llave));
	}

	/**
	 * Test 3: Verifies method delete(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * delete<br>
	 */
	@Test
	public void testDelete()
	{
		//Primer escenario con una linear probing HT
		setupEscenario1();

		linearHash.put(llave, taxi);
		linearHash.delete(llave);
		assertEquals(0, linearHash.size());
	}

	/**
	 * Test 4: Verifies method resize(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * resize<br>
	 */
	@Test
	public void testReHash()
	{
		//Primer escenario con una linear probing HT
		setupEscenario1();
		
		linearHash.put(llave, taxi);
		linearHash.resize(8);
		assertEquals(8, linearHash.capacity());
	}

	/**
	 * Test 5: Verifies method keys(). <br>
	 * <b>Methods to prove:</b> <br>
	 * put<br>
	 * keys<br>
	 */
	@Test
	public void testIter()
	{
		//Primer escenario con una linear probing HT
		setupEscenario1();

		linearHash.put(llave, taxi);
		assertEquals(llave, linearHash.keys().iterator().next());
	}
}

