package api;

import model.vo.Taxi;

import java.text.ParseException;

import model.data_structures.LinkedList;
import model.data_structures.SeparateChainingHash;
import model.vo.CompaniaServicios;
import model.vo.RangoFechaHora;
import model.vo.Service;

/**
 * Basic API for testing the functionality of the TaxiTrip manager
 */
public interface ITaxiTripsManager {

	/**
	 * Method to load the services of a specific taxi 
	 * The services are loaded in both a Stack and a Queue 
	 * @param servicesFile - path to the JSON file with taxi services
	 * @return true if the services are correctly loaded, false if not. 
	 */
	public boolean loadServices(String serviceFile);
	
	public LinkedList<Service> serviciosArea(int zone);		
}
