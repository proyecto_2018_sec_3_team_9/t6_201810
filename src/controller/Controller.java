package controller;

import java.text.ParseException;



import api.ITaxiTripsManager;
import model.data_structures.LinkedList;
import model.data_structures.SeparateChainingHash;
import model.logic.TaxiTripsManager;
import model.vo.CompaniaServicios;
import model.vo.RangoFechaHora;
import model.vo.Service;
import model.vo.Taxi;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager = new TaxiTripsManager();

	//1C
	public static boolean loadServices(String direccionJson)
	{
		return manager.loadServices(direccionJson);
	}
	
	public static LinkedList<Service> serviciosArea(int zone)
	{
		return manager.serviciosArea(zone);
	}

}
