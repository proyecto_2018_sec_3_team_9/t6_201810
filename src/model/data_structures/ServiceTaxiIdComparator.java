package model.data_structures;

import java.util.Comparator;

import model.vo.Service;
public class ServiceTaxiIdComparator implements Comparator<Service> {

	@Override
	public int compare(Service o1, Service o2) {
		// TODO Auto-generated method stub
		int comparar = o1.getTaxiId().compareTo(o2.getTaxiId());

		if(comparar > 0)
		{
			return 1;
		}
		else if(comparar < 0)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}

}
