package model.data_structures;

import java.util.Comparator;

/**
 * This class was created in base of the implementation of MergeSort that uses Arrays 
 * found on: https://algs4.cs.princeton.edu/22mergesort/Merge.java.html
 */
public class MergeSort<T>{

	// stably merge a[lo .. mid] with a[mid+1 ..hi] using aux[lo .. hi]
	private static void merge(LinkedList a, LinkedList aux, int lo, int mid, int hi, Comparator comparator, boolean direction) {
		// precondition: a[lo .. mid] and a[mid+1 .. hi] are sorted subarrays
		assert isSorted(a, lo, mid, comparator, direction);
		assert isSorted(a, mid+1, hi, comparator, direction);

		// copy to aux[]
		for (int k = lo; k <= hi; k++) {
			if(k > aux.size()-1) {
				aux.add(a.get(k));
			}
			else {
				aux.set(a.get(k), k);
			}
		}

		// merge back to a[]
		int i = lo, j = mid+1;
		for (int k = lo; k <= hi; k++) {
			if      (i > mid)              a.set(aux.get(j++), k);
			else if (j > hi)               a.set(aux.get(i++), k);
			else if (less(aux.get(j), aux.get(i), comparator,direction)) a.set(aux.get(j++), k);
			else                           a.set(aux.get(i++), k);
		}

		// postcondition: a[lo .. hi] is sorted
		assert isSorted(a, lo, hi, comparator, direction);
	}

	// mergesort a[lo..hi] using auxiliary array aux[lo..hi]
	private static void sort(LinkedList a, LinkedList aux, int lo, int hi, Comparator comparator, boolean direction) {
		if (hi <= lo) return;
		int mid = lo + (hi - lo) / 2;
		sort(a, aux, lo, mid, comparator, direction);
		sort(a, aux, mid + 1, hi, comparator, direction);
		merge(a, aux, lo, mid, hi, comparator, direction);
	}

	/**
	 * Rearranges the array in ascending order, using the natural order.
	 * @param a the array to be sorted
	 * comparator The comparator that will be used to know what atribute to use for comparing.
	 * direction The direction of the sorting. If true, it will be ascending. If false, it will be descending.
	 */
	public static void sort(LinkedList a, Comparator comparator, boolean direction) {
		LinkedList aux = new LinkedList();
		sort(a, aux, 0, a.size()-1, comparator, direction);
		assert isSorted(a, comparator, direction);
	}


	/***************************************************************************
	 *  Helper sorting function.
	 ***************************************************************************/

	// is v < w ?
	private static boolean less(Comparable v, Comparable w, Comparator comparator, boolean direction) {
		if(direction) {
			return comparator.compare(v, w) <= 0;
		}
		else {
			return comparator.compare(w, v) <= 0;
		}
	}

	/***************************************************************************
	 *  Check if array is sorted - useful for debugging.
	 ***************************************************************************/
	private static boolean isSorted(LinkedList a, Comparator comparator, boolean direction) {
		return isSorted(a, 0, a.size() - 1, comparator, direction);
	}

	private static boolean isSorted(LinkedList a, int lo, int hi, Comparator comparator, boolean direction) {
		for (int i = lo + 1; i <= hi; i++)
			if (less(a.get(i), a.get(i-1), comparator, direction)) return false;
		return true;
	}
}
