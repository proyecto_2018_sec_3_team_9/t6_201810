package model.vo;

import java.util.Date;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	/**
	 * Atribute with the taxi's unique id.
	 */
	private String taxi_id;
	
	private String company;
	
	/**
	 * Atribute with the taxi's number of services.
	 */
	private int services;
	
	/**
	 * Atribute with the distance in miles of the trip in the service.
	 */
	private double miles_total;
	
	/**
	 * Atribute with the total cost of the trip in the service.
	 */
	private double money_earned_total;
	
	
	public Taxi(String pTaxiId, String pCompany) {
		taxi_id = pTaxiId;
		company = pCompany;
	}
		
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId()
	{
		// TODO Auto-generated method stub
		return taxi_id;
	}
	
	public String getCompany()
	{
		return company;
	}
	
	public void setTaxiId(String pTaxiId)
	{
		this.taxi_id = pTaxiId;
	}
	
	public void setCompany(String pCompany)
	{
		this.company = pCompany;
	}

//	/**
//	 * @return services
//	 */
//	public int getNumServices()
//	{
//		// TODO Auto-generated method stub
//		return services;
//	}
//	
//	/**
//	 * @return miles - Distance of the trip in miles.
//	 */
//	public double getTotalMiles() {
//		// TODO Auto-generated method stub
//		return miles_total;
//	}
//	
//	/**
//	 * @return total - Total cost of the trip
//	 */
//	public double getTotalMoneyEarned() {
//		// TODO Auto-generated method stub
//		return money_earned_total;
	
	@Override
	public int compareTo(Taxi o) 
	{
		// TODO Auto-generated method stub
		int compare = -1;
		if(taxi_id.equals(o.getTaxiId())){
			compare = 0;
		}
		else if(taxi_id.compareTo(o.getTaxiId()) > 0) {
			return 1;
		}
		return compare;
	}
}