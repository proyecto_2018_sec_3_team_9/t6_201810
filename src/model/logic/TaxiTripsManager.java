package model.logic;

import java.io.File;


import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

import api.ITaxiTripsManager;
import model.data_structures.LinkedList;
import model.data_structures.MergeSort;
import model.data_structures.SeparateChainingHash;
import model.data_structures.ServiceDateComparator;
import model.data_structures.ServiceTaxiIdComparator;
import model.data_structures.TaxiIdComparator;
import model.vo.CompaniaServicios;
import model.vo.RangoFechaHora;
import model.vo.Service;
import model.vo.Taxi;
import model.vo.ZonaServicios;


public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large";


	private  LinkedList<Service> list1;

	private  LinkedList<Taxi> list2;

	private SeparateChainingHash<Integer, LinkedList<Service>> hashTable;

	@Override 
	public boolean loadServices(String direccionJson)
	{
		list1 = new LinkedList<Service>();

//		list2 = new LinkedList<Taxi>();
		
		SeparateChainingHash<Integer, LinkedList<Service>> hashTable = new SeparateChainingHash<>();
	
		System.out.println("Cargar sistema con" + direccionJson);

		JsonParser parser = new JsonParser();

		try 
		{
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
			Service[] service = gson.fromJson(new FileReader(direccionJson), Service[].class);
			for(Service s : service) 
			{
				list1.add(s);
//				Taxi taxi1 = new Taxi();
//				taxi1.setTaxiId(s.getTaxiId());
//				taxi1.setCompany(s.getCompany());
//				list2.add(taxi1);
			}
			
			LinkedList<Integer> listArea = new LinkedList<Integer>();
			for(int i = 0; i < list1.size(); i++)
			{
				Service actual = list1.get(i);
				int area = actual.getPickupArea();
				if(listArea.size() == 0)
				{
					listArea.add(area);
				}
				else
				{
					Boolean encontro = false;
					for(int j = 0; j <listArea.size(); j++)
					{
						int area2 = listArea.get(j);
						if(area2 == area)
						{
							encontro = true;
						}
					}
					if(!encontro)
					{
						listArea.add(area);
					}
				}
			}	
			
			for(int i = 0; i < listArea.size(); i++)
			{
				LinkedList<Service> servicios = new LinkedList<>();
				int pArea = listArea.get(i);
				for(int j = 0; j < list1.size(); j++)
				{
					Service actual = list1.get(j);
					if(actual.getPickupArea() == pArea)
					{
						servicios.add(actual);
					}
				}
				MergeSort.sort(servicios, new ServiceDateComparator(), true);	
				hashTable.put(pArea, servicios);
			}	
//			System.out.println("el tamaño de la lista de taxis es:" + list2.size());
			System.out.println("el tamaño de la lista de servicios es:" + list1.size());
		} 
		catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
		}	
		return false;
	}

	public LinkedList<Service> serviciosArea(int zone)
	{
		return hashTable.get(zone);
	}
}