package view;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Scanner;


import controller.Controller;
import model.data_structures.LinkedList;
import model.data_structures.SeparateChainingHash;
import model.logic.TaxiTripsManager;
import model.vo.CompaniaServicios;
import model.vo.RangoFechaHora;
import model.vo.Service;
import model.vo.Taxi;

/**
 * view del programa
 */
public class TaxiTripsManagerView 
{

	public static void main(String[] args) 
	{
		
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			//1C cargar informacion dada
			case 1:

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;
					break;

					//direccion json grande
				case 3:

					linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
					break;
				}

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
					Controller.loadServices(linkJson);
					
//					System.out.println("Hubo un problema con la carga del archivo");

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;

				//1A	
			case 2:
				
				//area
				sc.nextLine();
				System.out.println("Ingrese el valor del area de comunidad");
				String areaReq1 = sc.nextLine();
				
				int area = Integer.parseInt(areaReq1);
				
				LinkedList<Service> services = Controller.serviciosArea(area);
				for(int i = 0; i < services.size(); i++)
				{
					System.out.println("El id del servicio es:" + services.get(i).getTripId() + "El tiempo de llegada a la parada es:" + services.get(i).getEndTime());
				}	
				break;

			case 3: //2A
				
				break;

			case 4:
				fin=true;
				sc.close();
				break;
			}
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 6----------------------");
		System.out.println("Cargar data (1C):");
		System.out.println("1. Cargar toda la informacion dada una fuente de datos (small,medium, large).");

		System.out.println("\nFunciones:\n");
		System.out.println("2.Obtenga el id de un servicios y si tiempo de llegada a la parada dada un área de comunidad");
		System.out.println("3.Obtenga la lista de empresas con su información en el orden de su prioridad (de mayor a menor)");
		System.out.println("4. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");
	}

}
